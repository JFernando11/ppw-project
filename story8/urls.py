from django.urls import path
from . import views

app_name = 'story8'
urlpatterns = [
	path('story8', views.story8, name="story8"),
    path('result/', views.result, name="result"),
]