$(document).ready(function() {
    $.ajax({
        url: "/result?q=" + "Cara menjadi anak yang baik",
        success: function(data) {
            var header = `
            <thead>
                <tr>
                <th scope="col"> Judul </th>
                <th scope="col"> Penerbit </th>
                <th scope="col"> Gambar </th>
                </tr>
            </thead>
            `
            var array_items = data.items;
            console.log(array_items);
            $("#hasil").empty();
            $("#hasil").append(header);
            for (i = 0; i < array_items.length; i++) {
                var judul = array_items[i].volumeInfo.title;
                var penerbit = array_items[i].volumeInfo.authors;
                var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                var link = array_items[i].volumeInfo.infoLink;
                $("#hasil").append(`
                <tr>
                    <td> <a href=` + link + ` target='_blank'>` + judul + `</a> </td>
                    <td>` + penerbit + `</td>
                    <td><img src=` + gambar + ` class="img-thumbnail" alt="..." ></td>
                </tr>
                `);
            }
        }
    });

    $("#cari").keyup(function() {
        var input = $("#cari").val();
        $.ajax({
            url: "/result?q=" + input,
            success: function(data) {
                var header = `
                <thead>
                    <tr>
                    <th scope="col"> Judul </th>
                    <th scope="col"> Penerbit </th>
                    <th scope="col"> Gambar </th>
                    </tr>
                </thead>
                `
                var array_items = data.items;
                console.log(array_items);
                $("#hasil").empty();
                $("#hasil").append(header);
                for (i = 0; i < array_items.length; i++) {
                    var judul = array_items[i].volumeInfo.title;
                    var penerbit = array_items[i].volumeInfo.authors;
                    var gambar = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                    var link = array_items[i].volumeInfo.infoLink;
                    $("#hasil").append(`
                    <tr>
                        <td> <a href=` + link + ` target='_blank'>` + judul + `</a> </td>
                        <td>` + penerbit + `</td>
                        <td><img src=` + gambar + ` class="img-thumbnail" alt="..." ></td>
                    </tr>
                    `);
                }
            }
        });
    });

});