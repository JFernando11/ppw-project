from django.shortcuts import redirect, render
from django.http import JsonResponse
import requests
import  json

# Create your views here.

def story8(request):
    return render (request, 'story8.html')

def result(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET.get('q','frozen')
    ret = requests.get(url)
    data= json.loads(ret.content)
    return JsonResponse(data, safe=False)
