from django.shortcuts import redirect, render
from .forms import createMatkul
from .models import daftarMatkul
from django.http import HttpResponse, HttpResponseRedirect

def index(request):
	listMatkul = daftarMatkul.objects.all()
	return render(request, 'daftar.html', {'Daftar' : listMatkul})

def uploadMatkul(request):
	upload = createMatkul()
	if (request.method == 'POST'):
		upload = createMatkul(request.POST, request.FILES)
		if (upload.is_valid()):
			upload.save()
			return HttpResponseRedirect('/form')
	else:
		return render(request, 'form.html', {'upload_form' : upload})

def deleteMatkul(request, kode):
	mataKuliah = daftarMatkul.objects.get(id = kode)
	print(mataKuliah)
	mataKuliah.delete()
	return redirect('/form')

def home_view(request):
	return render(request, "home.html")

def profile_view(request):
	return render(request, "profile.html")