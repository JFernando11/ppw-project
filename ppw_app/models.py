from django.db import models

class daftarMatkul(models.Model):
	daftarSemester = (('Gasal 2019/2020', 'Gasal 2019/2020'),
		('Genap 2019/2020', 'Genap 2019/2020'), ('Gasal 2020/2021', 'Gasal 2020/2021'),
		('Genap 2020/2021', 'Genap 2020/2021'), ('Gasal 2021/2022', 'Gasal 2021/2022'),
		('Genap 2021/2022', 'Genap 2021/2022'), ('Gasal 2022/2023', 'Gasal 2022/2023'))
	mataKuliah = models.CharField(max_length = 50)
	namaDosen = models.CharField(max_length = 50)
	jumlahSKS = models.PositiveIntegerField(null = True)
	deskripsi = models.TextField(max_length = 50)
	semesterTahun = models.CharField(max_length = 50, choices = daftarSemester, null = True)
	ruangKelas = models.CharField(max_length = 50)

	def __str__(self):
		return self.mataKuliah