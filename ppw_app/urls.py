from django.urls import path
from . import views

app_name = 'ppw_app'
urlpatterns = [
	path('', views.home_view, name="home_url"),
	path('profile', views.profile_view, name="profile_url"),
	path('form/tambah', views.uploadMatkul, name="form_url"),
	path('form', views.index, name="index_url"),
	path('delete/<int:kode>', views.deleteMatkul, name="delete_url"),
]